package org.kotlinlang.play

class Customer //ชื่อคลาส

class Contact(val id: Int, var email: String) //ข้อมูลของคลาส

fun main() {

    val customer = Customer()

    val contact = Contact(1, "mary@gmail.com")

    println(contact.id)
    contact.email = "jane@gmail.com"
}