package org.kotlinlang.play

fun someCondition() = true // ถ้า someCondition() จริง

fun main() {
    val d: Int

    if (someCondition()) {  // จริง
        d = 1
    } else {   // ไม่จริง
        d = 2
    }

    println(d)   // ปริ้นจริง
}